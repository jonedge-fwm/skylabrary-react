/* props */

export const dummyRows = [1, 2, 3, 4, 5]

export const dummyColumns = {
  Multiple: row => row,
  '-Five': row => 5,
  Product: row => row * 5,
}

export const dummyRowKeyFunction = row => row
