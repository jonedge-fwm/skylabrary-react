/* global test, it, describe, expect */
import React from 'react'
import {render, screen, within, cleanup} from 'test-utils'
import '@testing-library/jest-dom'

import Table from '.'
import {dummyRows, dummyColumns, dummyRowKeyFunction} from './dummy'

it('renders with no errors', () => {
  render(<Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
})

describe('prop handling', () => {

  it('requires `rows`, `columns` and `rowKeyFunction` props', () => {
    // this test probably isn't needed, typescript should do it for us
    return false
  })

  it('renders a row for each item in the `rows` prop', () => {
    render(<Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
    expect(screen.getAllByRole('row').length).toBe(dummyRows.length + 1) // +1 for the header row
    cleanup()
    render(<Table rows={[1, 2, 3, 4, 5, 6, 7, 8]} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
    expect(screen.getAllByRole('row').length).toBe(8 + 1) // +1 for the header row
  })

  it('renders a header row using the `column` prop\'s keys for headers', () => {
    render(<Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
    const headerRow = screen.getAllByRole('row')[0]
    const headerCells = within(headerRow).getAllByRole('columnheader')
    expect(headerCells[0]).toHaveTextContent('Multiple')
    expect(headerCells[1]).toHaveTextContent('Five')
    expect(headerCells[2]).toHaveTextContent('Product')
    expect(headerCells.length).toBe(3)
  })
  
  it('ensures that `column` prop keys beginning with a hyphen are only visible to screen readers', () => {
    render(<Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
    const headerRow = screen.getAllByRole('row')[0]
    const headerCells = within(headerRow).getAllByRole('columnheader')
    expect(headerCells[1].firstChild).toHaveClass('sr-only')
    expect(headerCells[0].firstChild).not.toHaveClass('sr-only')
    expect(headerCells[2].firstChild).not.toHaveClass('sr-only')
  })

  it('omits leading hyphens from `column` prop keys when rendering them as headers', () => {
    render(<Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
    const headerRow = screen.getAllByRole('row')[0]
    const headerCells = within(headerRow).getAllByRole('columnheader')
    expect(headerCells[1]).toHaveTextContent('Five')
    expect(headerCells[1]).not.toHaveTextContent('-Five')
  })
  
  it('for every row, renders a cell for each item in the `columns` prop', () => {
    render(<Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} />)
    const dataRow = screen.getAllByRole('row')[2]
    const dataCells = within(dataRow).getAllByRole('cell')
    expect(dataCells[0]).toHaveTextContent('2')
    expect(dataCells[1]).toHaveTextContent('5')
    expect(dataCells[2]).toHaveTextContent('10')
    expect(dataCells.length).toBe(3)
  })

})

describe('styling', () => {

  it('changes the background-color of every second row if the `striped` prop is true', () => {
    return false
  })

})
