import { ComponentStory, ComponentMeta } from '@storybook/react';

import Table from '.';
import {dummyRows, dummyColumns, dummyRowKeyFunction} from './dummy'

export default {
  title: 'Components/Table',
  component: Table,
} as ComponentMeta<typeof Table>;

export const Basic: ComponentStory<typeof Table> = () => (
  <Table rows={dummyRows} columns={dummyColumns} rowKeyFunction={dummyRowKeyFunction} striped />
)
