import React from 'react'
import styled from '@emotion/styled'

export default styled(Table)`
  --baseBackground: #FFFFFF;
  --stripedBackground: #DDDDDD;

  ${props => props.striped ? `
    tbody tr {
      background-color: var(--baseBackground);
      &:nth-child(odd) {
        background-color: var(--stripedBackground);
      }
    ` : null}
  }
`

export function Table(props) {
  return (
    <table className={props.className}>
      <thead>
        <tr>
          {Object.keys(props.columns).map((columnHeading) => (
            <th scope="col" key={columnHeading}>
              {columnHeading.charAt(columnHeading) === '-'
                ? <span className="sr-only">{columnHeading.substring(1)}</span>
                : <span>{columnHeading}</span>
              }
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {props.rows?.map(row => (
          <tr key={props.rowKeyFunction(row)}>
            {Object.keys(props.columns).map((columnHeading) => (
              <td key={columnHeading}>{props.columns[columnHeading](row)}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}
