module.exports = {
  "stories": [
    "../app/**/stories.@(js|jsx|ts|tsx)",
    "../app/**/*.stories.@(js|jsx|ts|tsx)",
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    {
      name: '@storybook/addon-postcss',
      options: {
        cssLoaderOptions: {
          importLoaders: 1, // When CSS is split over multiple files using @import()
        },
        postcssLoaderOptions: {
          implementation: require('postcss'), // When using postCSS 8
        },
      },
    },
  ],
  "framework": "@storybook/react"
}